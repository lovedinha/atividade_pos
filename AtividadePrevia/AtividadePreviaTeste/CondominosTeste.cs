﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AtividadePrevia;

namespace AtividadePreviaTeste
{
    [TestClass]
    public class CondominosTeste
    {
        [TestMethod]
        public void AutorizaEvento_Teste()
        {
            int qtdMesEsperado = 3;

            Condominos cn = new Condominos("José Antônio", 207, 2);



            // Essa função soma 1 mês ao saldo devedor de meses existente do côndomino que estiver em débito
            cn.AutorizarEvento(false);

            // assert
            int qtdMesAtualizado = cn.QtdMeses;
            Assert.AreEqual(qtdMesEsperado, qtdMesAtualizado, 0.001, "Quantidade de meses não foi atualizada corretamente.");
        }
    }
}
