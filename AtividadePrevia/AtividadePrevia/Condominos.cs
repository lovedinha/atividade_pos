﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtividadePrevia
{
    public class Condominos
    {
        private string nome;
        private int unidade;
        private int qtdMeses;
        private bool condominioPago = false;

        private Condominos()
        {
        }

        public Condominos(string NomeCondomino, int UnidadeCondomino, int TotMeses)
        {
            nome = NomeCondomino;
            unidade = UnidadeCondomino;
            qtdMeses = TotMeses;
        }

        public string Nome
        {
            get { return nome; }
        }
        public int Unidade
        {
            get { return unidade; }
        }

        public int QtdMeses
        {
            get { return qtdMeses; }
        }



        public void AutorizarEvento(bool CondominioPago)
        {

            if (CondominioPago && qtdMeses < 3)
            {
                Console.WriteLine("Condômino habilitado para aluguel de salão de festas e/ou churrasqueiras.");
            }
            else
            {
                if (!CondominioPago)
                {
                    qtdMeses += 1;
                }
                if (qtdMeses > 3)
                {
                    throw new Exception("Condômino restrito a áreas pagas, pois há mais de 3 meses em débito.");
                }
            }

        }

        public static void Main()
        {
            Condominos cn = new Condominos("Lidia Roza", 256, 2);
            cn.AutorizarEvento(false);
        }
    }
}
